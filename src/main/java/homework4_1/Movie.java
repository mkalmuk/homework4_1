package homework4_1;

public class Movie {
	
	private String rank;
	private String title;
	private String studio;
	private String worldwide;
	private String domestic_money;
	private String domestic_percent;
	private String overseas_money;
	private String overseas_percent;
	private String year;
	
	public Movie(String rank, String title, String studio, String worldwide,
			String domestic_money, String domestic_percent, String overseas_money, 
			String overseas_percent, String year) {
		super();
		this.rank = rank;
		this.title = title;
		this.studio = studio;
		this.worldwide = worldwide;
		this.domestic_money = domestic_money;
		this.domestic_percent = domestic_percent;
		this.overseas_money = overseas_money;
		this.overseas_percent = overseas_percent; 
		this.year = year;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStudio() {
		return studio;
	}

	public void setStudio(String studio) {
		this.studio = studio;
	}

	public String getWorldwide() {
		return worldwide;
	}

	public void setWorldwide(String worldwide) {
		this.worldwide = worldwide;
	}

	public String getDomestic_money() {
		return domestic_money;
	}

	public void setDomestic_money(String domestic_money) {
		this.domestic_money = domestic_money;
	}

	public String getDomestic_percent() {
		return domestic_percent;
	}

	public void setDomestic_percent(String domestic_percent) {
		this.domestic_percent = domestic_percent;
	}

	public String getOverseas_money() {
		return overseas_money;
	}

	public void setOverseas_money(String overseas_money) {
		this.overseas_money = overseas_money;
	}

	public String getOverseas_percent() {
		return overseas_percent;
	}

	public void setOverseas_percent(String overseas_percent) {
		this.overseas_percent = overseas_percent;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	@Override
	public String toString() {
		return "Movie [rank=" + rank+", title= "+title+", studio= "+studio+", worldwide="+
				worldwide+ ", domestic money= " + domestic_money+", domestic percent= "+ domestic_percent+
				", overseas money= "+ overseas_money+", overseas_percent= "+ overseas_percent+
				", year="+ year;
		
	}
	
}
