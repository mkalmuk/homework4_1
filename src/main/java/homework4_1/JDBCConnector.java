package homework4_1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class JDBCConnector {

	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
	
	static final String DB_URL = "jdbc:mysql://localhost/movies"
			+ "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	static final String USER = "root";
	static final String PASS = "1111";
	
	public static void main(String[] args) {
		Connection cntn = null;
		Statement stmt = null;
		try {
			// STEP 2: Register JDBC driver
			Class.forName(JDBC_DRIVER);

			// STEP 3: Open a connection
			System.out.println("Connecting to database...");
			cntn = DriverManager.getConnection(DB_URL, USER, PASS);

			// STEP 4: Execute a query
			System.out.println("Creating statement...");
			stmt = cntn.createStatement();
			String sql;
			sql = "SELECT * FROM top_movies";
			ResultSet rs = stmt.executeQuery(sql);

			// STEP 4.1
			List<Movie> moviesList = new ArrayList<Movie>();

			// STEP 5: Extract data from result set
			while (rs.next()) {
				// Retrieve by column name
				moviesList.add(new Movie(rs.getString("rank"), rs.getString("title"),
						rs.getString("studio"), rs.getString("worldwide"), 
						rs.getString("domestic_money"), rs.getString("domestic_percent"), 
						rs.getString("overseas_money"),	rs.getString("overseas_percent"), 
						rs.getString("year")));
						}
			// STEP 5.1
			for (Movie movie : moviesList) {
				System.out.println(movie);
			}

			// STEP 6: Clean-up environment
			rs.close();
			stmt.close();
			cntn.close();
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			// close resources
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
				se2.printStackTrace();
			}
			
			try {
				if (cntn != null) {
					cntn.close();
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		System.out.println("Goodbye!");
	}
}
